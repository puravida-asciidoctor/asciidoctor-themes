package com.puravida.asciidoctor;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.extension.JavaExtensionRegistry;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

public class AsciidoctorThemesRegistry implements ExtensionRegistry {

    @Override
    public void register(Asciidoctor asciidoctor) {
        asciidoctor.javaExtensionRegistry().preprocessor(ThemesPreProcessor.class);
        asciidoctor.javaExtensionRegistry().postprocessor(ThemesPostProcessor.class);
    }
}
